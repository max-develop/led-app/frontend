$(document).ready(function () {

  //react to changes from the slider
  $('.sub').on('click', function () {
    var obj = {
      ID: "48498095",
      Name: $(".card-title").html(),
      DeviceType: "blinkt",
      Values: {
        R: $('input[name=slider-r]')[0].value,
        G: $('input[name=slider-g]')[0].value,
        B: $('input[name=slider-b]')[0].value,
        A: Number.parseFloat($('input[name=slider-a]')[0].value).toFixed(1) * 0.1 + "",
      }
    }
    console.log(Number.parseFloat($('input[name=slider-a]')[0].value).toFixed(1) * 0.1);
    console.log(obj)

    $("#name").val(
      $(this)
        .closest(".card-content")
        .find(".card-title")[0].innerText
    );
    console.log($('#rgb-form').serialize());

    var url = "http://frontend-rest:80/frontend-api/update-rgb"

    $.ajax({
      type: "PUT",
      url: url,
      crossDomain: true,
      data: JSON.stringify(obj),
      dataType: "text",
      success: function (response) {

      }
    });

  });

  $(".switch").change(function (e) {
    e.preventDefault();
    if (!$(this).is(":checked")) {
      console.log("ayy");
    }
  });

});